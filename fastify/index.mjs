import Fastify from 'fastify'
import fastifyHttpProxy from '@fastify/http-proxy'
//import process from "node:process";
const fastify = Fastify({
  logger: true
})


fastify.register(fastifyHttpProxy, {
  upstream: 'http://localhost:5050',
  prefix: '/hello', // optional
  http2: false // optional
})

fastify.register(fastifyHttpProxy, {
  upstream: 'http://localhost:5060',
  prefix: '/hey', // optional
  http2: false // optional
})


fastify.listen({ port: 3000 })